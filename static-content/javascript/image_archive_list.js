/* js for the image archive image list */
$(function(){
	
	$('#loading').hide();
	
	
	/* not using sessions at this time */
	/*
	if($.cookie('last_viewed_images')){
		// load all images stored in the session...
	}
	
	// check for the last viewed plan image
	if($.cookie('last_plan_image')){
		// display the last viewed plan image
		var plan_href = $.cookie('last_plan_image');
		$('#planImage').attr('src', plan_href);
	}
	*/
	
	// when the "view plan" link is clicked, display the new image, then store it in the session
	$('.planImageLink').click(function(){
		var plan_href = $(this).attr('href');
		//$.cookie('last_plan_image', plan_href);
		$('#planImage').attr('src', plan_href);
		return false;
	});
	
	$('.archiveImageList table tr td img').click(function(){
		
		var img = $(this).attr('src').replace('.thum.jpg', '.jpg');
		//img = img.replace('thum/','');
		
		var id = img.replace(/\/|\./g, '');
		
		if( $('#' + id).attr('src') ){
			$('#' + id).parent().fadeIn();
			$('#' + id).ScrollTo(1000);
		}else{
			
			$('#loading').fadeIn();
			
			// start loading the new image
			$('#imageArchiveDetailImages').image(img, function(){
				// image is done loading...
				
				// hide the loading message
				$('#loading').fadeOut();
				
				// create a new placeholder for the image
				$(this).wrap('<div></div>');
				
				// attach it to the DOM
				$(this).parent().prepend(
					'<div style="text-align:right;"><a href="#top" name="' + id + '">BACK TO TOP</a></div>'
				).find('a').click(function(){
					$('#top').ScrollTo(1000);
					return false;
				})
				
				// fade the image out if clicked
				$(this).click(function(){
					$(this).parent().fadeOut();
				});
				
				// scroll to the bottom of the page to help show the image
				$('#bottom').ScrollTo(1000);
				
			}, function(new_img){
					new_img.attr('id', id);
					new_img.css('display', 'block');
					new_img.css('margin', 'auto');
					new_img.css('vertical-align', 'middle');
				}
			);
			
		}
	})
})