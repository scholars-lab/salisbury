/* js for the computer model tour pages */
$(function(){
	// preload "over" images
	$("img.tourModelRolloverImage").each(function(i) {
		temp = this.src;
		pre = temp.replace('space/full/image', 'space/full/wire')
		preload_image_object = new Image();
		preload_image_object.src = pre;
	});
	// create "hover" event handler
	$("img.tourModelRolloverImage").hover(
		function() {
			curr = $(this).attr("src");
			over = curr.replace('space/full/image', 'space/full/wire')
			$(this).attr({src:over});
		},
		function() {
			$(this).attr({src:curr});
		}
	)
});