/**
 * Load an image into an element:
 * $('#imageContainer').image('logo.gif', function onComplete(){}, function onStart(image){})
*/
$.fn.image = function(_src, f, pre_load){
  return this.each(function(){
    var img = $('<img />').appendTo(this);
	if(pre_load){
		pre_load(img);
	}
	img.attr('src', _src).load( f );
  });
};