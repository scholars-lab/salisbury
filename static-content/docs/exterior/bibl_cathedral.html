<span id='archive-banner'>This site has been archived as static HTML pages. Search and other functionality are no longer availble.</span>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="generator" content=
"HTML Tidy for Mac OS X (vers 1st December 2004), see www.w3.org" />
<title>Cathdral: Bibliography</title>
</head>
<body bgcolor="#FFFFFF" link="#990000" vlink="#663300" alink=
"#000000">
<a name="top" id="top"></a>
<table width="571">
<tr>
<td colspan="2" width="571"><img border="0" src=
"./layout/bibl.gif" usemap="#banner" alt="menu" /> <map name=
"banner" id="banner">
<area shape="rect" coords="1,0,97,44" href="./" />
<area shape="rect" coords="107,46,188,62" href="cathedral.html" />
<area shape="rect" coords="194,46,253,62" href="close.html" />
<area shape="rect" coords="261,46,319,62" href="town.html" />
<area shape="rect" coords="327,46,431,62" href="parish.html" />
<area shape="rect" coords="439,46,508,62" href="sarum.html" />
</map></td>
</tr>
</table>
<br />
<table>
<tr>
<td>
<dl>
<dd>
<h5><font size="+1" color="#996600">CATHEDRAL</font></h5>
</dd>
<dt><u>Salisbury Cathedral perspectives on the Architectural
History</u>, by Thomas Cocke and Peter Kidson (London, 1993).</dt>
<dd>"Historical Summary" by Thomas Cocke summarizes the early years
of building then chronicles the changes made to the cathedral from
1220 to the 1980's. In "The Historical Circumstances and the
Principles of the Design" Peter Kidson provides his own
interpretation of some of the information provided by Cocke and
goes on to discuss the underlying mathematical system of the
design. Handsome black and white illustrations. Fold-out plan of
the cathedral - the most accurate to date. The plan is
indispensible.<br />
<br /></dd>
<dt><u>Sumptuous and richly adorn'd: the decoration of Salisbury
Cathedral</u>, Sarah Brown (London, 1999).</dt>
<dd>The final volume on the cathedral includes sepulchral
monuments, church fittings, glass and tiles.<br />
<br /></dd>
<dt><u>Wiltshire</u> by Nicolas Pevsner, revised by Bridget Cherry.
Penguin Books Ltd, Harmondsworth, Middlesex, England, 1975,
reprinted 1981.</dt>
<dd>The Buildings of England series.<br />
<br /></dd>
<dt><u>Medieval Art and Architecture at Salisbury Cathedral</u>.
Edited by Laurence Keen and Thomas Cocke. The British
Archaeological Association Conference Transactions XVII. W.S. Maney
and Son Limited, Leeds, 1996.</dt>
<dd>Contains a small version of the RCHME plan of the cathedral.
Twelve articles include Diana Greenway on St. Osmund, Gavin Simpson
on dendrochronological evidence, Peter Draper and Virginia Jansen
on cathedral architecture, M.F. Hearn and Lee Willis on the Lady
Chapel, Richard Morris on buttressing, Tim Tatton-Brown on the
spire, Pamel Blum on sculpture in the chapter house, James King on
sculpture at Old Sarum, Freda Anderson on tomb-slabs, Christopher
Norton on pavements and Richard Marks on stained glass.<br />
<br /></dd>
<dt><u>A History of Wiltshire</u>, 15 volumes to date. (Victoria
County History of England.) Vol. III 1956: The Religious Houses of
Wiltshire</dt>
<dd>Contains chapters on ecclesiastical history from 1087 to 1956,
Roman Catholicism and Protestant Nonconformity, along with
individual chapters on the cathedral, abbeys, priories, friaries,
hospitals and colleges in Wiltshire. Substance is mainly
documentary history. Excellent source book. Few illustrations.</dd>
<dd>
<h5><font size="+1" color="#996600">ARCHITECTURE</font></h5>
</dd>
<dt>"The Sequence of the Building Campaigns at Salisbury," by
Pamela Z. Blum, <u>Art Bulletin</u>, LXXIII (1991), 6-37.<br />
<br /></dt>
<dt><u>Salisbury Cathedral.</u> Text by Canon A.F. Smethurst.
Pitkin Pictorials Limited, London, 1990. Pamphlet with excellent
color photographs and a brief historical text.<br />
<br /></dt>
<dt><u>Salisbury Cathedral a landmark in England's heritage</u> by
Roy Spring, edited by Jane Drake. Shell Publicity Services, London,
1991.<br />
<br /></dt>
<dt><u>Salisbury Cathedral</u> by Roy Spring. New Bell's Cathedral
Guides. Unwin Hyman, London, 1987.</dt>
<dd>Excellent guide but unfortunately out-of-print.<br />
<br /></dd>
<dt><u>The Story of Salisbury Cathedral</u> by Rena Gardiner.
Workshop Press, Tarrant Monkton, Dorset, 1994.</dt>
<dd>Drawings and brief historical text. The only publication that
gives some indication, albeit over-enthusiastically, of the color
that once graced Salisbury Cathedral inside and out.<br />
<br /></dd>
<dt><u>Mark of the Mason at Salisbury Cathedral</u> by Roy Spring.
RJK Smith and Associates, Much Wenlock, 1995. Former clerk of the
works discusses mason's marks.</dt>
<dd>Good color photographs.<br />
<br /></dd>
<dt><u>Salisbury Cathedral: the West Front. A History and Study in
Conservation</u>. Edited by Tim Ayers. Phillimore and Co., Ltd.,
Chichester, 2000.</dt>
<dd>
<h5><font size="+1" color="#996600">SCULPTURE</font></h5>
</dd>
<dt>'The Middle English romance, Iacob and Iosep," and the Joseph
cycle of the Salisbury chapter house,' by Pamela Z Blum,
<u>Gesta</u> 8, 18-34.<br />
<br /></dt>
<dt><u>The Salisbury chapter-house and its Old Testament cycle: An
archeological and iconographical study</u>. By Pamela Z. Blum,
Ph.D. dissertation, Yale University (1978). U.M.I. 8121401.<br />
<br /></dt>
<dt>"The sculptures of the Salisbury chapter house," <u>Medieval
art and architecture at Salisbury cathedral</u>. BAA Conference
Transactions 17, L. Keen and T. Cocke (eds.) Leeds, 1996,
68-78.<br />
<br /></dt>
<dt>"The Tomb of Giles de Bridport in Salisbury Cathedral," by
Marion Roberts, <u>Art Bulletin</u>, LXV (1983), 559-586.<br />
<br /></dt>
<dt><u>The Chapter House Salisbury Cathedral</u>. Text by Canon
David Durston, 1995.</dt>
<dd>Clear but small photographs of individual scenes of the Old
Testment carved in relief on the interior of the chapter house. No
general pictures of the interior, however.</dd>
<dd>
<h5><font size="+1" color="#996600">STAINED GLASS</font></h5>
</dd>
<dt>"Thirteenth-Century Glass of the Salisbury Chapter House," by
Pamela Z. Blum, <u>Gesta</u> 37, 142-149.<br />
<br /></dt>
<dt><u>Stained Glass Salisbury Cathedral</u> by Roy Spring. RJK
Smith and Associates, Much Wenlock, 1997.</dt>
<dd>Pamphlet inventory of stained glass in the cathedral from the
middle ages to the present. One wishes it were fully
illustrated.</dd>
<dd>
<h5><font size="+1" color="#996600">LITURGY</font></h5>
</dd>
<dt>"Liturgical influences on the design of the west front at Wells
and Salisbury," by Pamela Z. Blum, <u>Gesta</u> 25, 145-150.<br />
<br /></dt>
<dt><u>The Processions of Sarum and the Western Church</u> by
Terence Bailey, 1971.<br wp="br1" />
<br wp="br2" /></dt>
<dd>
<h5><font size="+1" color="#996600">PEOPLE</font></h5>
</dd>
<dt><u>Saint Osmund of Salisbury</u> by Daphne Stroud. RJK Smith
and Associates, Much Wenlock, 1994. Pamphlet.</dt>
<dd>Biography of Salisbury Cathedral's only saint by a noted
historian.<br />
<br /></dd>
<dt><u>Richard Poore and the building of Salisbury Cathedral</u> by
Daphne Stroud. Cathedral Print Services Ltd, Salisbury, 1996.</dt>
<dd>Biographical essay of the first bishop of the
thirteenth-century cathedral.<br />
<br /></dd>
<dt><u>Elias of Dereham Architect of Salisbury Cathedral</u> by
Adrian Hastings. RJK Smith and Associates, Much Wenlock, 1997.</dt>
<dd>Useful discussion of the first clerk of the works at Salisbury
Cathedral written by a professor of religious studies. Elias was an
administrator, not an architect or (more properly) a master
mason.<br />
<br /></dd>
<dt><u>The Jewel of Salisbury</u> by Clifford Dobson. RJK Smith and
Associates, Much Wenlock, 1996. Biographical essay about Bishop
John Jewel (1522-1571).<br />
<br /></dt>
<dt><u>The English Secular Cathedrals in the Middle Ages</u> by
Kathleen Edwards, 2nd.ed. Barnes and Noble, New York, 1967.</dt>
<dd>Indispensible discussion of the English secular cathedrals with
chapters on the canons, bishop, major and minor officials of the
cathedral church.</dd>
</dl>
</td>
</tr>
</table>
<br />
<p><a href="#top">Back to Top</a></p>
<!-- Matomo --> <script> var _paq = window._paq = window._paq || []; /* tracker methods like "setCustomDimension" should be called before "trackPageView" */ _paq.push(['trackPageView']); _paq.push(['enableLinkTracking']); (function() { var u="https://analytics.lib.virginia.edu/"; _paq.push(['setTrackerUrl', u+'matomo.php']); _paq.push(['setSiteId', '47']); var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s); })(); </script> <!-- End Matomo Code --></body>
</html>
