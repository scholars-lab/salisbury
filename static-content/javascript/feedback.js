$(function(){
	$('#feedbackForm').submit(function(){
		var email = $('#from').attr('value');
		if( ! email ){
			alert('Your email address is required.');
			$('#from').focus();
			return false;
		}
		var emailx  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if( ! email.match(emailx) ){
			alert('Please enter a valid email address.');
			$('#from').focus();
			return false;
		}
		if( ! $('#message_body').attr('value') ){
			alert('A message is required.');
			$('#message_body').focus();
			return false;
		}
	})
})