/* js for the ajax (*not currently used*) image archive menu */

function closeSiblingUls(a){
	// close sibling li.ul's
	liSiblings = a.parent().siblings();
	liSiblingULs = liSiblings.children('ul');
	if(liSiblingULs.length){
		liSiblingULs.hide('fast');
		liSiblings.find('a.treeNode').attr('state', 0)
	}
}

function toggleNode(a){
	if( a.attr('state') != 1 ){
		// open child ul's
		a.attr('state', 1);
		a.siblings('ul').show('fast');
		closeSiblingUls(a);
	}else{
		a.attr('state', 0);
		a.siblings('ul').hide('fast');
	}
}

function treeNodeClick(e){
	var a = $(this);
	var li = a.parent();
	var id = a.attr('id');
	
	if( a.attr('loaded') ){
		toggleNode(a);
	}else{
		// set the loaded flag immediately to prevent double requests
		a.attr('loaded', 1);
		$.get("/salisbury/archive_menu_fragment", { sub: id }, function(data){
			li.append(data);
			li.find('ul').hide();
			toggleNode(a);
			li.find('ul li a.treeNode').click(treeNodeClick);
			newTreeNodeLoaded(a);
		});
	}
	//e.disableDefaultEvent();
	return false;
}

function newTreeNodeLoaded(a){
	
}

$(function(){
	$("body").ajaxError(function(request, settings){
		$(this).append("<li>Error requesting page " + settings.url + "</li>");
	});
	
	$('a.treeNode').click(treeNodeClick);
});