This scrapes the old salisbury site built with Apache Cocoon and Solr, and converts it to plain HTML/CSS and Solr(?) or maybe some other search product.

To scrape the old site:
- `wget --mirror --no-host-directories --no-parent --page-requisites --convert-links --convert-file-only --adjust-extension --directory-prefix=static-content http://salisbury.art.virginia.edu`

The search functionality was deeply imbedded into this site, such that when
converting to static HTML pages, the search form is on 550+ HTML pages. This
would require the script tag be inserted on and made to work with each of these
pages. It's programatically easy to insert the tag, but funcitonally
impractical to check each page. With the conversion to static HTML other
functionality was lost, such as some of the building tour ability.

# Solr Search
The solr container is available. The following was done to the original data to
make it work with the latest version of solr (7).

Changed field types:
- textTight => string
- text => text_general
- sfloat => pfloat
- sint => pint

Changed field name:
- text => fulltext


# Static version
Ultimately, this site will be served as static HTML with a banner noting the limited functionality from the original. No search, and broken building tour.


# Production
- This should just serve the static files from nginx or other web server. No docker needed.
